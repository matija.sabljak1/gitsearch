//
//  DetailsRowViewModel.swift
//  GitSearch
//
//  Created by Matija Sabljak on 05.05.2022..
//

import Foundation

import Foundation

class DetailsRowViewModel {
    
    var repositoryName: String?
    var updateTime: String?
    var owner: String?
    var description: String?
    
    init(repositoryName: String?, updateTime: String?, owner: String?, description: String?) {
        self.repositoryName = repositoryName
        self.updateTime = updateTime
        self.owner = owner
        self.description = description
    }
}
