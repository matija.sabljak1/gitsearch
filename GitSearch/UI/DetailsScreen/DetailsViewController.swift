//
//  DetailsViewController.swift
//  GitSearch
//
//  Created by Matija Sabljak on 04.05.2022..
//

import UIKit

class DetailsViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: DetailsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
    }
    
    func setupTable(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "DetailsTableViewCell", bundle: .main), forCellReuseIdentifier: "DetailsTableViewCell")
    }
}

extension DetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.tableModel.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DetailsTableViewCell", for: indexPath) as? DetailsTableViewCell else {
            return UITableViewCell()
        }
        
        cell.viewModel = viewModel.tableModel[indexPath.row]

        return cell
    }
}

