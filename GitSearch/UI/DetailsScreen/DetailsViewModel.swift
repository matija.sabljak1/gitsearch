//
//  DetailsViewModel.swift
//  GitSearch
//
//  Created by Matija Sabljak on 04.05.2022..
//


import Foundation
import UIKit

class DetailsViewModel: BaseViewModel {
    
    var tableModel:[DetailsCellViewModel] = []
    
    init(item: GitItem){
        tableModel.append(DetailsCellViewModel.init(title: "Repository Name:", subtitle: item.name))
        tableModel.append(DetailsCellViewModel.init(title: "Last Updated Time:", subtitle: item.updated_at?.toDate().toString()))
        tableModel.append(DetailsCellViewModel.init(title: "Owner:", subtitle: item.owner?.login))
        tableModel.append(DetailsCellViewModel.init(title: "Description:", subtitle: ""))
    }
}
