//
//  DetailsCellViewModel.swift
//  GitSearch
//
//  Created by Matija Sabljak on 05.05.2022..
//

import Foundation

class DetailsCellViewModel {
    
    var title: String?
    var subtitle: String?
    
    init(title: String?, subtitle: String?) {
        self.title = title
        self.subtitle = subtitle
    }
}
