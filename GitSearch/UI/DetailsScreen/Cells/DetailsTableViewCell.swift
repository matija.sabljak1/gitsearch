//
//  DetailsTableViewCell.swift
//  GitSearch
//
//  Created by Matija Sabljak on 05.05.2022..
//

import UIKit

class DetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    
    var viewModel: DetailsCellViewModel? {
        didSet {
            self.title.text = viewModel?.title
            self.subtitle.text = viewModel?.subtitle
        }
    }
}
