//
//  HomeViewController.swift
//  GitSearch
//
//  Created by Matija Sabljak on 04.05.2022..
//

import UIKit

class HomeViewController: BaseViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    private var activityView = UIActivityIndicatorView(style: .large)
    
    var viewModel: HomeViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        setupTable()
        setupCallbacks()
        setupActivity()
        navigationItem.title = "Git Hub Search"
    }
    
    func setupTable(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "GitItemTableViewCell", bundle: .main), forCellReuseIdentifier: "GitItemTableViewCell")
    }    
    
    func setupCallbacks() {
        viewModel.onDataFetched = { [weak self] in
            guard let self = self else { return }
            self.tableView.reloadData()
            self.activityView.stopAnimating()
        }
    }
    
    func setupActivity() {
        activityView.center = view.center
        activityView.hidesWhenStopped = true
    }
}

extension HomeViewController: UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        guard let query = searchBar.text else { return }
        viewModel.fetchData(query: query)
        activityView.startAnimating()
        view.addSubview(activityView)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            viewModel.clearTable()
            tableView.reloadData()
        }
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumberOfRows()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 52.0
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GitItemTableViewCell", for: indexPath) as? GitItemTableViewCell else {
            return UITableViewCell()
        }
        
        let gitItem = viewModel.getGitItem(row: indexPath.row)
        
        cell.viewModel = GitItemTableViewModel(name: gitItem?.name, updateTime: gitItem?.updated_at?.toDate().toString())

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.moveToDetails(row: indexPath.row)
    }
}




