//
//  GitItemTableViewModel.swift
//  GitSearch
//
//  Created by Matija Sabljak on 04.05.2022..
//

import Foundation

class GitItemTableViewModel {
    
    var name: String?
    var updateTime: String?
    
    init(name: String?, updateTime: String?) {
        self.name = name
        self.updateTime = updateTime
    }
}
