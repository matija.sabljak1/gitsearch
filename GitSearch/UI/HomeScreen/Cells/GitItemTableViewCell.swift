//
//  GitItemTableViewCell.swift
//  GitSearch
//
//  Created by Matija Sabljak on 04.05.2022..
//

import UIKit

class GitItemTableViewCell: UITableViewCell {
    
    var moveToDetailsScreen: ((Int) -> Void)?
    
    var viewModel: GitItemTableViewModel? {
        didSet {
            self.name.text = viewModel?.name
            self.updateTime.text = viewModel?.updateTime
        }
    }

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var updateTime: UILabel!
    
}
