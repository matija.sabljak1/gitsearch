//
//  HomeViewModel.swift
//  GitSearch
//
//  Created by Matija Sabljak on 04.05.2022..
//

import Foundation

class HomeViewModel: BaseViewModel {
    
    var moveToDetailsScreen: ((GitItem) -> Void)?
    var onDataFetched: EmptyCallback?
    
    private var gitItem: GitItem?
    
    private var data: Data? {
        didSet {
            sortedData = data?.items?.sorted(by: {
                $0.updated_at?.toDate().compare($1.updated_at?.toDate() ?? Date()) == .orderedAscending
            })
        }
    }
    
    private var sortedData: [GitItem]?

    func fetchData(query: String) {
        APIManager.searchGitHub(query: query, onComplete: { [weak self] (response) in
                guard let self = self else { return }
                switch response {
                case .success(let data):
                    self.data = data
                    self.onDataFetched?()
                case .failure(let error):
                    self.onError?(error.error.message)
                }
        })
    }
    
    func getGitItem(row: Int)-> GitItem? {
        guard let sortedData = sortedData else { return nil }
        return sortedData[row]
    }
    
    func getNumberOfRows() -> Int {
        guard let sortedData = sortedData else { return 0 }
        return sortedData.count
    }
 
    func moveToDetails(row: Int){
        guard let item = sortedData?[row] else { return }
        self.moveToDetailsScreen?(item)
    }
    
    func clearTable(){
        sortedData = []
    }
}
