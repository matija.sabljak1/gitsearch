//
//  Owner.swift
//  GitSearch
//
//  Created by Matija Sabljak on 04.05.2022..
//

import Foundation

class Owner: Codable {
    var id: Int?
    var login: String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case login
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.login = try container.decode(String.self, forKey: .login)
    }
}
