//
//  GitItem.swift
//  GitSearch
//
//  Created by Matija Sabljak on 04.05.2022..
//

import Foundation

class GitItem: Codable {
    var id: Int?
    var name: String?
    var updated_at: String?
    var owner: Owner?
    //var description: String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case updated_at
        case owner
        //case description
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.updated_at = try container.decode(String.self, forKey: .updated_at)
        self.owner = try container.decode(Owner.self, forKey: .owner)
        //self.description = try container.decode(String.self, forKey: .description)
    }
}
