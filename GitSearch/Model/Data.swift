//
//  Data.swift
//  GitSearch
//
//  Created by Matija Sabljak on 04.05.2022..
//

import Foundation

class Data: Codable {
    var items: [GitItem]?
    
    private enum CodingKeys: String, CodingKey {
        case items
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.items = try container.decode([GitItem].self, forKey: .items)
    }
}
