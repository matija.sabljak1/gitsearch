//
//  Config.swift
//  GitSearch
//
//  Created by Matija Sabljak on 03.05.2022..
//

import Foundation

struct Config {
    struct Url {
        static var baseURL = "https://api.github.com"
    }
}
