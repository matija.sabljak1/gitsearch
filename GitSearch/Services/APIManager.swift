//
//  APIManager.swift
//  GitSearch
//
//  Created by Matija Sabljak on 03.05.2022..
//


import Foundation
import Alamofire

class APIManager: NSObject {

    static func searchGitHub(query: String, onComplete: @escaping (NetworkResponse<Data>) -> Void) {
        AF.request(APIRouter.search(query)).responseData { (response) in
            let statusCode = response.response?.statusCode ?? 0000
            switch response.result {
            case .success(let data):
                guard let items: Data = try? JSONDecoder().decode(Data.self, from: data) else {
                    onComplete(.failure(NetworkError(message: ErrorMessage.parse)))
                    return
                }
                onComplete(.success(items))
            case .failure(_):
                onComplete(.failure(NetworkError(message: ErrorMessage.failure(code: statusCode))))
            }
        }
    }
}
