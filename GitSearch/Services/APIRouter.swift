//
//  APIRouter.swift
//  GitSearch
//
//  Created by Matija Sabljak on 03.05.2022..
//

import Foundation
import Alamofire

enum APIRouter: URLRequestConvertible {
    
    case search(String)
    
    var path: String {
        switch self {
        case .search:
            return "/search/repositories"
        }
    }
        
    var method: HTTPMethod {
        switch self {
        case .search:
            return .get
        }
    }
        
    var parameters: [String: Any] {
        switch self {
        case .search(let query):
            let params: [String: Any] = ["q": query]
            return params
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = URL(string: Config.Url.baseURL)
        let urlRequest = try URLRequest(url: (url?.appendingPathComponent(path))!, method: method)
        print(urlRequest)
        return try URLEncoding.default.encode(urlRequest, with: parameters)
    }
}
