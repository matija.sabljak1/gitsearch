//
//  Networking.swift
//  GitSearch
//
//  Created by Matija Sabljak on 03.05.2022..
//

import Foundation

enum NetworkResponse<T: Decodable> {
    case success(T)
    case failure(NetworkError)
}
