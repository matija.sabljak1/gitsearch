//
//  StatusCode.swift
//  GitSearch
//
//  Created by Matija Sabljak on 03.05.2022..
//

import Foundation

enum StatusCode: Int {
    case validation_failed = 422
    case unauthorized = 401
}
