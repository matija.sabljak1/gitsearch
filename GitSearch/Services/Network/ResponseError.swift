//
//  ResponseError.swift
//  GitSearch
//
//  Created by Matija Sabljak on 03.05.2022..
//

import Foundation

struct ResponseError: Codable {
    var message: String
    
    init(message: String) {
        self.message = message
    }

}
