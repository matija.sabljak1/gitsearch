//
//  NetworkError.swift
//  GitSearch
//
//  Created by Matija Sabljak on 03.05.2022..
//

import Foundation

class NetworkError: Codable {
    var error: ResponseError
    
    init(message: String) {
        error = ResponseError(message: message)
    }
}
