//
//  BaseViewModel.swift
//  GitSearch
//
//  Created by Matija Sabljak on 04.05.2022..
//

import Foundation

public typealias EmptyCallback = () -> Void

class BaseViewModel {
    var onComplete: EmptyCallback?
    var onError: ((String) -> Void)?
    var onShowAlertMessage: ((String, String) -> Void)?
}
