//
//  Coordinator.swift
//  GitSearch
//
//  Created by Matija Sabljak on 04.05.2022..
//

import Foundation
import UIKit

protocol Coordinator {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    
    func start()
}
