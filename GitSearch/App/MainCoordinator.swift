//
//  MainCoordinator.swift
//  GitSearch
//
//  Created by Matija Sabljak on 04.05.2022..
//

import Foundation
import UIKit

class MainCoordinator: Coordinator {
    var childCoordinators: [Coordinator] = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController){
        self.navigationController = navigationController
    }
    
    func start() {
        navigationController.pushViewController(createHomeViewController(), animated: false)
    }
    
    func createHomeViewController() -> UIViewController {
        let vc = HomeViewController.instance()
        let viewModel = HomeViewModel()
        vc.viewModel = viewModel
        
        viewModel.moveToDetailsScreen =  { [weak self] item in
            guard let self = self else { return }
            let vc = DetailsViewController.instance()
            let viewModel = DetailsViewModel(item: item)
            vc.viewModel = viewModel
            self.navigationController.pushViewController(vc, animated: true)
        }
        
        vc.viewModel = viewModel
        return vc
    }
}
